#!/usr/bin/env python3
import json
import requests

email = 'Nathan@yesenia.net'
album_id = 3
url_users_service = 'https://jsonplaceholder.typicode.com/users'
url_posts_service_by_user_id = 'https://jsonplaceholder.typicode.com/posts?userId='
url_albums_service_by_user_id = 'https://jsonplaceholder.typicode.com/albums?id=3&userId='
url_photos_service_by_user_id = 'https://jsonplaceholder.typicode.com/photos?userId='

r = requests.get(url=url_users_service)
users = r.json()
# print('users', users)

user = list(filter(lambda user: user['email'] == email, users))[0]
user_id = str(user['id'])
# print('user_id', user_id)

posts = requests.get(url=url_posts_service_by_user_id + user_id).json()
albums = requests.get(url=url_albums_service_by_user_id + user_id).json()
photos = requests.get(url=url_photos_service_by_user_id + user_id).json()
# print('posts', posts)
# print('albums', albums)
# print('photos', photos)

response = {
    'user': user,
    'posts': posts,
    'albums': albums,
    'photos': photos
}
response = json.dumps(response)
print(response)
